#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	List mylist;
	//push to tail
	mylist.pushToHead('k');
	mylist.pushToTail('e');
	mylist.pushToTail('n');
	mylist.print();

	//pop tail
	mylist.popTail();
	mylist.print();

	//pop head
	mylist.popHead();
	mylist.print();

	//search
	mylist.search('e');

	//TO DO! Complete the functions, then write a program to test them.
	//Adapt your code so that your list can store data other than characters - how about a template?
	system("Pause");
}