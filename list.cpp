#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head, 0);
	if (tail == 0)
	{
		tail = head;
	}
	else
	{        
		head->next->prev = head;
	}
}
void List::pushToTail(char el)
{
	Node *tmp = new Node(el);
	if (head == 0)
	{
		head = tail = new Node(el); // there is only 1 node , head = tail = el
	}
	else
	{
		tail->next = tmp;  // add new last node
		tmp->prev = tail;
		tail = tmp;  // tail point to new last node
		tail->next = NULL;
		
	}
}

char List::popHead()
{
	char el = head->data;
	Node *tmp = head; // temp = old head
	if (head == tail)
	{
		head = tail = NULL;
	}
	else
	{
		head = head->next; // new head = next node
		head->prev = NULL;
	}
	delete tmp; // delete old head
	return el;
}
char List::popTail()
{
	char el = tail->data;
	Node *tmp = tail;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		tail = tail->prev;
		tail->next = NULL;

	}
	delete tmp;
	return el;
}
bool List::search(char el)
{
	Node *curr = head;
	Node *prev1 = NULL;
	Node *prev2 = NULL;
	while (curr != NULL) // check all data in the list
	{
		if (curr->data == el) { cout << el << " is in the list." << endl; break; }
		else { cout << el << " is not in the list." << endl; break; }
	}
	
	return NULL;
}
void List::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
	cout << endl;
}